package com.rsrit.dataops.sqlserver.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rsrit.dataops.sqlserver.dao.SqlServerHostDao;
import com.rsrit.dataops.sqlserver.entity.SqlServerHost;
import com.rsrit.dataops.sqlserver.operations.InitiateSqlServerInstallation;

@RestController
@RequestMapping(value= "/installation")
public class InstallationController {
	
	@Autowired
	SqlServerHostDao sqlServerHostDao;
	
	InitiateSqlServerInstallation sqlServerInstallation;
	
	public InstallationController(InitiateSqlServerInstallation sqlServerInstallation) {
		super();
		this.sqlServerInstallation = sqlServerInstallation;
	}
	
	@RequestMapping(value="{project_id}/sqlserver", method= RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void initiateToolInstallation(@RequestBody SqlServerHost sqlServerHostCredentials,
			@PathVariable("project_id") Long projectId) {
		System.out.println("Entered in installation controller");
		
		//save Sql Server host credentials
		sqlServerHostDao.saveSqlServerHostCredentials(sqlServerHostCredentials, projectId);
		
		//installing Sql in the host IP
		sqlServerInstallation.initateSqlServerInstallation(sqlServerHostCredentials, projectId);
	}
}

