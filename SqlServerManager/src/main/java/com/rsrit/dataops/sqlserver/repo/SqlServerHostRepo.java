package com.rsrit.dataops.sqlserver.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rsrit.dataops.sqlserver.entity.SqlServerHost;

@Repository
public interface SqlServerHostRepo extends JpaRepository<SqlServerHost, Long> {
	SqlServerHost findByHostIp(String hostIp);
}
