package com.rsrit.dataops.sqlserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqlServerManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SqlServerManagerApplication.class, args);
	}
}
