package com.rsrit.dataops.sqlserver.operations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import com.rsrit.dataops.sqlserver.dao.SqlServerHostDao;
import com.rsrit.dataops.sqlserver.entity.SqlServerHost;

@Component
@EnableAsync
public class InitiateSqlServerInstallation {

	@Autowired
	SqlServerHostDao sqlServerDao;
	
	@Async
	public void initateSqlServerInstallation(SqlServerHost templateCredentials, Long projectId) {
		System.out.println("Entered in initiate sqlserver installation method");
		if(templateCredentials.getHostToolType().equals("sqlserver")) { 
			try {
				//Invoking SqlServer Installation Scripts
				invokeSqlServerInstallation(templateCredentials, projectId);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void invokeSqlServerInstallation(SqlServerHost templateCredentials, Long projectId) throws IOException {
		
		System.out.println("Installing sql server ......!");
		//installation Sql server
		
		ClassPathResource pathForScriptFile = new ClassPathResource("SqlServerScripts");

		// Invoke Ansible script for sql server

		ProcessBuilder processBuilderForSQLServerInstallations = new ProcessBuilder();
		processBuilderForSQLServerInstallations
			.directory(pathForScriptFile.getFile());
		
		processBuilderForSQLServerInstallations
			.command("sh","sqlservershell.sh",
					templateCredentials.getHostIp(),
					templateCredentials.getHostUsername(),
					templateCredentials.getHostPassword(),
					templateCredentials.getHostPassword());
		
		/*
		.command("/bin/bash","ANSIBLE_HOST_KEY_CHECKING=False", "ansible-playbook",
				"sqlserver.yml", "-i", "\'"+instanceIp+"\'", "-e", 
				"\'ansible_ssh_user="+userName, "ansible_ssh_pass="+password,
				"ansible_sudo_pass="+password,"sa_password="+password+"\'");
				*/
		
		Process process = processBuilderForSQLServerInstallations.start();

		BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line = null;
		while ((line = inputReader.readLine()) != null) {
			System.out.println(line);
		}
		BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

		while ((line = errorReader.readLine()) != null) {
			System.out.println(line);
		}
		
		try {
			process.waitFor();
			if (process.exitValue() == 0) {
				this.sqlServerDao.updateStatusOfInstallationByHostIp(templateCredentials.getHostIp(), 1);
				System.out.println("Sql server installation is successfully completed in " + templateCredentials.getHostIp());
			} else {
				this.sqlServerDao.updateStatusOfInstallationByHostIp(templateCredentials.getHostIp(), 1);
				System.out.println("Process exited with code " + process.exitValue());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
