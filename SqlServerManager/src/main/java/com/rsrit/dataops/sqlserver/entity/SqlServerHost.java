package com.rsrit.dataops.sqlserver.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "host")
public class SqlServerHost {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String hostIp;
	private String hostUsername;
	private String hostPassword;
	private String hostToolType;
	
	private int status;
	private Long projectId;

	public SqlServerHost() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SqlServerHost(String hostIp, String hostUsername, String hostPassword, String hostToolType) {
		super();
		this.hostIp = hostIp;
		this.hostUsername = hostUsername;
		this.hostPassword = hostPassword;
		this.hostToolType = hostToolType;
	}

	public String getHostIp() {
		return hostIp;
	}
	
	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}

	public String getHostUsername() {
		return hostUsername;
	}

	public void setHostUsername(String hostUsername) {
		this.hostUsername = hostUsername;
	}

	public String getHostPassword() {
		return hostPassword;
	}

	public void setHostPassword(String hostPassword) {
		this.hostPassword = hostPassword;
	}

	public String getHostToolType() {
		return hostToolType;
	}

	public void setHostToolType(String hostToolType) {
		this.hostToolType = hostToolType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	@Override
	public String toString() {
		return "SqlServerHost [id=" + id + ", hostIp=" + hostIp + ", hostUsername=" + hostUsername + ", hostPassword="
				+ hostPassword + ", hostToolType=" + hostToolType + ", status=" + status + ", projectId=" + projectId
				+ "]";
	}

}
