package com.rsrit.dataops.sqlserver.dao;

import com.rsrit.dataops.sqlserver.entity.SqlServerHost;

public interface SqlServerHostDao {

	public void saveSqlServerHostCredentials(SqlServerHost sqlserverHostCredentials, Long projectId);
	public void updateStatusOfInstallationByHostIp(String hostIp, int status);
	public SqlServerHost findByHostIp(String hostIp);
}
