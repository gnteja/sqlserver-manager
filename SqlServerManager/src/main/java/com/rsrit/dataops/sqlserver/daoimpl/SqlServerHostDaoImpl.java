package com.rsrit.dataops.sqlserver.daoimpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rsrit.dataops.sqlserver.dao.SqlServerHostDao;
import com.rsrit.dataops.sqlserver.entity.SqlServerHost;
import com.rsrit.dataops.sqlserver.repo.SqlServerHostRepo;

@Service
@Transactional
public class SqlServerHostDaoImpl implements SqlServerHostDao {
	
	@Autowired
	SqlServerHostRepo sqlserverHostCredentialsRepo;
	
	@Override
	public void saveSqlServerHostCredentials(SqlServerHost sqlServerHostCredentials, Long projectId) {
		if(this.sqlserverHostCredentialsRepo.findByHostIp(sqlServerHostCredentials.getHostIp()) != null) {
			System.out.println("the Host Ip already exists");
		}else {
			if(projectId !=null) {
				sqlServerHostCredentials.setProjectId(projectId);
				this.sqlserverHostCredentialsRepo.save(sqlServerHostCredentials);
			}else {
				System.out.println("projectId cannot be null");
			}
		}
	}

	@Override
	public void updateStatusOfInstallationByHostIp(String hostIp, int status) {
		System.out.println("updating the status of sql server installation");
		SqlServerHost hostCredentials = this.sqlserverHostCredentialsRepo.findByHostIp(hostIp);
		hostCredentials.setStatus(status);
		this.sqlserverHostCredentialsRepo.save(hostCredentials);
	}

	@Override
	public SqlServerHost findByHostIp(String hostIp) {
		System.out.println("Sql Server credentials using Host IP");
		return this.sqlserverHostCredentialsRepo.findByHostIp(hostIp);
	}

}
